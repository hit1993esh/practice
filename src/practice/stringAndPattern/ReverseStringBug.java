package practice.stringAndPattern;

//  10. Reverse String Bug

public class ReverseStringBug {
	public static void main(String[] args) {
		System.out.println(reverseString("abecd"));
	}
	private static String reverseString(String line) {

		char[] input = line.toCharArray();

		int left = 0, right = input.length - 1;

		while (left < right) {

			char tmp = input[left];
			input[left++] = input[right];
			input[right--] = tmp;
		}

		return String.valueOf(input);
	}

}
