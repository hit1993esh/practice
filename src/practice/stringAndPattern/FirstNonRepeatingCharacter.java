package practice.stringAndPattern;

// 4. First NonRepeating Character

public class FirstNonRepeatingCharacter {
	public static void main(String[] args) {

		System.out.println(findFirst("annnghasjdfghsdf"));
	}

	private static char findFirst(String input) {

		char[] letters = input.toCharArray();

		int hash[] = new int[26];

		for (int i = 0; i < letters.length; i++) {

			hash[letters[i] % 97]++;
		}

		for (int i = 0; i < letters.length; i++) {

			if (hash[letters[i] % 97] == 1)
				return letters[i];
		}

		return '0';
	}
}
