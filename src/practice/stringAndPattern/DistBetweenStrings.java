package practice.stringAndPattern;

// 1. Dist. Between Strings 

public class DistBetweenStrings {

	public static void main(String[] args) {

		double result = shortestDistance("In publishing and graphic design, lorem ipsum is a filler text "
				+ "commonly used to demonstrate the graphic elements", "is", "a");
		System.out.println(result);

	}

	public static double shortestDistance(String doc, String one, String two) {

		String[] docs = doc.split("[, ]");
		double word1Distance = 0, word2Distance = 0;
		double totalDistance = 0;
		double minLength = doc.length() + 1;

		for (String word : docs) {
			if (word.equals(one)) {
				word1Distance = totalDistance + (word.length() / 2d);
			} else if (word.equals(two)) {
				word2Distance = totalDistance + (word.length() / 2d);
			}
			if (word1Distance > 0 && word2Distance > 0) {
				double current = Math.abs(word1Distance - word2Distance);
				if (current < minLength) {
					minLength = current;
				}
			}
			totalDistance += word.length() + 1;
		}
		return minLength;
	}

}
