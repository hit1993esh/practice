package practice.stringAndPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 5. Group Anagrams

public class GroupAnagrams {

	public static void main(String args[]) {

		List<String> givenList = Arrays.asList("cat", "dog", "god");
		
		Map<String, List<String>> mp = group(givenList);
		
		mp.entrySet().forEach(e -> {
			System.out.println(e.getValue());
		});
	}

	private static Map<String, List<String>> group(List<String> words) {

		Map<String, List<String>> mp = new HashMap<String, List<String>>();

		for (String word : words) {
			String hash = getHash(word);
			if (mp.containsKey(hash)) {
				mp.get(hash).add(word);
			} else {
				List<String> value = new ArrayList<String>();
				value.add(word);
				mp.put(hash, value);
			}
		}
		return mp;
	}

	private static String getHash(String word) {
		
		char [] charArr = word.toCharArray();
		Arrays.sort(charArr);
		return String.valueOf(charArr);
	}
}
