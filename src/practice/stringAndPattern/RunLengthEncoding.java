package practice.stringAndPattern;

// 7. Run Length Encoding

public class RunLengthEncoding {
	public static void main(String[] args) {

		System.out.println(encodedString("hhhdddddsssssaaaaavcdddddd"));

	}

	private static String encodedString(String line) {

		String result = "";
		int index = 0;
		char[] input = line.toCharArray();
		int count = 1;
		result += input[index];
		for (int i = 1; i < input.length; i++) {

			if (input[i] != input[index]) {
				index = i;
				result += String.valueOf(count);
				result += input[index];
				count = 1;
			} else
				count++;
		}

		result += String.valueOf(count);

		return result;
	}

}
