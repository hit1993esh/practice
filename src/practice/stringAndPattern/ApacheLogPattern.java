package practice.stringAndPattern;

// 3. Apache Log Pattern

import java.util.HashMap;
import java.util.Map;

public class ApacheLogPattern {
	public static void main(String[] args) {

		String[] input = { "10.1.1.1 - sajhaskjhkjsad", "10.1.1.1 - sajhaskjhkjsad", "10.1.1.1 - sajhaskjhkjsad",
				"10.1.1.2 - sajhaskjhkjsad", "10.1.1.2 - sajhaskjhkjsad" };

		System.out.println(findTopIpAddr(input));

	}

	private static String findTopIpAddr(String[] lines) {

		HashMap<String, Integer> ipCount = new HashMap<String, Integer>();
		int maxCount = 0;

		for (int i = 0; i < lines.length; i++) {

			String ip = lines[i].split(" - ")[0];

			if (ipCount.get(ip) == null) {
				ipCount.put(ip, 1);
			} else {
				ipCount.put(ip, 1 + ipCount.get(ip));
			}

			if (maxCount < ipCount.get(ip))
				maxCount = ipCount.get(ip);

		}

		String result = "";
		for (Map.Entry<String, Integer> keys : ipCount.entrySet()) {

			if (maxCount == keys.getValue()) {
				result += keys.getKey() + ",";
			}
		}

		return result.substring(0, result.length() - 1);
	}
}
