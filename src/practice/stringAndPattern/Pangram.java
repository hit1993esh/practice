package practice.stringAndPattern;

// 8. Pangram

public class Pangram {
	public static void main(String[] args) {

		System.out.println(findMissingLetters("abcd fgsr hijk mlonpqrst uvwx yz"));

	}

	private static String findMissingLetters(String line) {

		char[] input = line.toCharArray();
		String result = "";

		int[] hash = new int[26];

		for (int i = 0; i < input.length; i++) {

			if (input[i] != ' ') {
				hash[input[i] % 97]++;
			}

		}

		for (int i = 0; i < hash.length; i++) {
			if (hash[i] == 0)
				result += String.valueOf((char) (i + 97));

		}

		return result;
	}
}
