package practice.mathematics;

// 2. Dot Product

public class DotProduct {
	public static void main(String args[]) {
		int[] arr1 = new int[] { 2, 3 };
		int[] arr2 = new int[] { 3, 4, 6 };
		int result = dotProduct(arr1, arr2);
		System.out.println("Result : " + result);
	}

	private static int dotProduct(int[] arr1, int[] arr2) {
		int result = 0;
		int product = arr1[0];
		for (int i = 1; i < arr1.length; i++) {
			product *= arr1[i];
		}
		result += product;
		product = arr2[0];
		for (int i = 1; i < arr2.length; i++) {
			product *= arr2[i];
		}
		result += product;
		return result;
	}
}
