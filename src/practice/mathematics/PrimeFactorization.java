package practice.mathematics;

import java.util.ArrayList;

// 5. Prime Factorization 

public class PrimeFactorization {

	public static void main(String args[]) {

		int num = 12;
		ArrayList<Integer> result = primeFactorization(num);

		result.forEach(res -> System.out.print(" " + res));
	}

	private static ArrayList<Integer> primeFactorization(int num) {
		ArrayList<Integer> result = new ArrayList<Integer>();

		for (int i = 2; i <= Math.sqrt(num); i++) {
			while (num % i == 0) {
				result.add(i);
				num /= i;
			}
		}
		if (num > 2)
			result.add(num);
		return result;
	}

}
