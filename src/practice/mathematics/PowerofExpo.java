package practice.mathematics;

// Power of Expo ( Math.pow) 

public class PowerofExpo {
	public static void main(String args[]) {

		int num = 100;
		double result = power(num, 2);
		System.out.println("Result : " + result);
	}

	private static double power(double num, int exp) {
		double result = 1;

		for (int i = 1; i <= exp; i++)
			result *= num;

		return result;
	}
}
