package practice.mathematics;

// 3. Is Power of 10

public class IsPowerof10 {
	public static void main(String args[]) {

		int num = 100;
		boolean result = isPowerOf10(num);
		System.out.println("Result : " + result);
	}

	private static boolean isPowerOf10(int num) {
		return (num % 10 == 0);
	}

}
