package practice.mathematics;

import java.util.HashMap;
import java.util.Map;

// 7. Decimal Conversion
public class DecimalConversion {
	public static void main(String[] args) {
		String result = vulgarToDecimal(Long.valueOf(1), Long.valueOf(30));
		System.out.println(result);
		result = vulgarToDecimal(Long.valueOf(1), Long.valueOf(75));
		System.out.println(result);
		result = vulgarToDecimal(Long.valueOf(4), Long.valueOf(7));
		System.out.println(result);
	}

	private static String vulgarToDecimal(Long numerator, Long denominator) {

		String retString = "";
		retString += String.valueOf(numerator / denominator);
		// System.out.println(retString);
		Map<Integer, Integer> hash = new HashMap<Integer, Integer>();
		int rem = Long.valueOf(numerator % denominator).intValue();
		String decimal = "";
		while (rem != 0 && !hash.containsKey(rem)) {
			hash.put(rem, decimal.length());
			rem *= 10;
			decimal += String.valueOf(rem / denominator);
			rem %= denominator;
		}

		if (rem == 0)
			return retString + "." + decimal;
		else if (hash.containsKey(rem))
			return retString + "." + decimal.substring(0, hash.get(rem)) + "(" + decimal.substring(hash.get(rem)) + ")";
		else
			return retString;
	}

}
