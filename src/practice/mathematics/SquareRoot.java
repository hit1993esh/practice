package practice.mathematics;

// 6. Square root

public class SquareRoot {
	public static void main(String[] args) {
		double result = squreRoot(10);
		System.out.println(result);
	}

	private static double squreRoot(int num) {
		return Math.sqrt(num);
	}

}
