package practice.mathematics;

// 1. Add Fraction

public class AddFraction {
	public static void main(String args[]) {
		int[] fraction1 = new int[] { 2, 3 };
		int[] fraction2 = new int[] { 3, 4 };
		int[] result = addFractions(fraction1, fraction2);
		System.out.println("Result : " + result[0] + "/" + result[1]);
	}

	private static int[] addFractions(int[] fraction1, int[] fraction2) {
		int[] result = new int[] { fraction1[0] * fraction2[1] + fraction1[1] * fraction2[0],
				fraction1[1] * fraction2[1] };
		return result;
	}
}
