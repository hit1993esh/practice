package practice.miscellaneous;

import java.util.Stack;

public class Snowpack {
	public static void main(String args[]) {
		System.out.println(computeSnowpack(new Integer [] { 0,1,3,0,1,2,0,4,2,0,3}));
	}

	public static  Integer computeSnowpack(Integer[] arr)  {
		
		Integer ans = 0;
		
		Stack<Integer> index = new Stack<Integer>();
		
		for(int i =0 ; i < arr.length ; i++) {
			
			while(!index.isEmpty() && arr[index.peek()] < arr[i]) {
				
				int lastIndex = index.peek();
				
				index.pop();
				
				if( ! index.isEmpty()) {
					
					int dis = i - index.peek() - 1 ;
					ans += dis * (
							Math.min(arr[index.peek()], arr[i]) - arr[lastIndex]);
				}
				
			}
			index.push(i);
		}
		
		return ans;
	}
}
