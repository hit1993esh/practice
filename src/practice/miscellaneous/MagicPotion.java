package practice.miscellaneous;

public class MagicPotion {
	
	public static void main(String args[]) {
		
		System.out.println(minimalSteps("ABCABCEABCABCED"));
		System.out.println(minimalSteps("ABCDABCE"));
		System.out.println(minimalSteps("AAAABCABABCD"));
	}

	private static Integer minimalSteps(String ingredients) {
		
		int count = 1;
		int i = 1;
		while(i < ingredients.length()) {
			
			if(ingredients.length() >= 2*i && ingredients.charAt(i) == ingredients.charAt(0)) {
				//System.out.println(ingredients.substring(0, i) + " " +
				//		ingredients.substring(i, i + i));
				if(ingredients.substring(0, i)
						.equals(ingredients.substring(i, 2*i))) {
					i += i-1;
				}
			}
			count++;
			i++;
		}
		
		return count;
	}
	
}
