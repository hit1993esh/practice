package practice.miscellaneous;

public class CountLengthOfCycle {

	public static void main(String args[]) {
		int arr [] = {1, 0};
			int	startIndex = 0;
			System.out.println(countLengthofcycle(arr, startIndex));
	}
	public static int countLengthofcycle(int [] arr, int startIndex){
		int index = startIndex, count = 0;
		
		int cycleCounter[] = new int[arr.length];
		
		while(cycleCounter[index] == 0){
			cycleCounter[index] = ++count;
			index = arr[index];
		}
		return count - cycleCounter[index];
	}
}
