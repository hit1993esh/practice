package practice.miscellaneous;

public class PascalsTriangle {

	public static void main(String[] args) {
			
		System.out.println(pascal(4, 0));
		System.out.println(pascal(4, 1));
		System.out.println(pascal(4, 2));
		System.out.println(pascal(4, 3));
		System.out.println(pascal(4, 4));
	}
	
	public static Integer pascal(Integer i, Integer j) {
		
		if(i <= 1  || j == 0 || i == j) return 1;
		
		return pascal(i -1, j - 1) + pascal (i - 1, j);
		
	}

}
