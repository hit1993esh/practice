package practice.miscellaneous;

import java.util.HashSet;
import java.util.Set;

public class UniqueTuples {
	public static void main(String[] args) {
		String input = "abbccde";
		System.out.println(uniqueTuples(input, 2));
	}

	private static Set<String> uniqueTuples(String input, int len) {
		Set<String> result = new HashSet<>();

		for (int i = 0; i < input.length() - len + 1; i++) {
			result.add(input.substring(i, i + len));
		}

		return result;
	}
}
