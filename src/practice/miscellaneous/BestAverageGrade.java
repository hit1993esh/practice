package practice.miscellaneous;

import java.util.HashMap;
import java.util.Map;

public class BestAverageGrade {

	public static void main(String[] args) {
		String[][] scores =  { { "Bobby", "87" },
				{ "Charles", "100" },
				{ "Eric", "64" },
				{ "Charles", "22" } };
		System.out.println(bestAverageGrade(scores));
	}

	public static Integer bestAverageGrade(String[][] scores) {
		
		Map<String, String> totalMarks = new HashMap<String, String>();
		
		for(int  i = 0 ; i < scores.length ; i++) {
			if(totalMarks.containsKey(scores[i][0])) {
				totalMarks.put(scores[i][0], 
						totalMarks.get(scores[i][0]) + "," + scores[i][1]);
			}else {
				totalMarks.put(scores[i][0], scores[i][1]);
			}
		}
		
		System.out.println(totalMarks);
		
		Integer maxAvg = 0;
		for(Map.Entry<String, String> map : totalMarks.entrySet()) {
			
			String [] marks = map.getValue().split(",");
			int sum = 0;
			for(int i = 0 ; i < marks.length ; i ++) {
				sum += Integer.valueOf(marks[i]); 
			}
			maxAvg = Math.max(maxAvg, sum/marks.length);
		}
		
		return maxAvg;
	}
}
