package practice.arrays;

public class MedianTwoSortedArrays {

	public static void main(String args[]) {
		int[] arr1 = new int[] { 1, 3 , 6 };
		int[] arr2 = new int[] { 2, 4 , 5 };
		System.out.println(findMedianSortedArrays(arr1, arr2));
	}

	public static double findMedianSortedArrays(int[] arr1, int[] arr2) {

		int arr1Index = 0, arr2Index = 0;
		int n = arr1.length;
		int m1 = -1, m2 = -1;

		for (int i = 0; i <= n; i++) {

			if (arr1Index == n) {
				return (arr1[arr1Index - 1] + arr2[0]) / 2d;
			}
			if (arr2Index == n) {
				return (arr2[arr2Index - 1] + arr1[0]) / 2d;
			}

			if (arr1[arr1Index] <= arr2[arr2Index]) {
				m1 = m2;
				m2 = arr1[arr1Index++];
			} else {
				m1 = m2;
				m2 = arr2[arr2Index++];
			}
		}
		return (m1 + m2) / 2d;
	}
}
