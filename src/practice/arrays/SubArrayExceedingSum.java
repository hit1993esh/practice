package practice.arrays;

public class SubArrayExceedingSum {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 4, 1, 2, 1, 6, 6};
		System.out.println(subArrayExceedsSum(arr, 12));
	}

	public static int subArrayExceedsSum(int arr[], int target) {

		int n = arr.length;
		int right = 0, left = 0;
		int curr_sum = 0;
		int min_length = Integer.MAX_VALUE;
		while (right < n) {

			while (curr_sum < target && right < n) {
				curr_sum += arr[right++];
			}
			while (curr_sum >= target && left < n) {
				if (curr_sum == target) {
					if (min_length > right - left) {
						min_length = right - left;
					}
				}
				curr_sum -= arr[left++];
			}
		}

		if (min_length == Integer.MAX_VALUE)
			return -1;

		return min_length;
	}

}
