package practice.numericProblems;

// Second Smallest

public class SecondSmallest {
	public static void main(String[] args) {
	    
	      int[] arr = new int[] {-1, 0, 1, -3, -2};
	     System.out.println(findSecondSmallest(arr));
	    
	    int[] arr2 = new int[] {0, 1};
	     System.out.println(findSecondSmallest(arr2));
	      
	    }
	  
	  private static int findSecondSmallest(int [] arr){
	    
	    if(arr.length < 2) return 0;
	    
	    int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;
	    
	    for(int i = 0 ; i < arr.length ; i ++){
	      
	          if(arr [i] < min1 ) {
	            min2 = min1;
	            min1 = arr [i];
	          }
	          if(min2 > arr[i] && arr[i] > min1) min2 = arr[i]; 
	    }
	    
	    return min2;
	  }
	
}
