package practice.numericProblems;

// 1. Smallest Number

public class SmallestNumber {
	public static void main(String[] args) {

		int[] arr = new int[] { 3, 4, 5, 6, 1, 2};
		System.out.println(findMinimum(arr));

		int[] arr2 = new int[] { 2, 1 };
		System.out.println(findMinimum(arr2));
	}

	private static int findMinimum(int[] arr) {

		int left = 0, right = arr.length - 1;
		int mid = (left + right) / 2;

		while (left < right) {

			if (mid > left && arr[mid] < arr[mid - 1]) {
				return arr[mid];
			}
			if (mid < right && arr[mid] > arr[mid + 1]) {
				return arr[mid + 1];
			}

			if (arr[mid] > arr[right])
				left = mid;
			else
				right = mid;

			mid = (left + right) / 2;
		}

		if (left > right)
			return arr[0];
		else if (left == right)
			return arr[left];
		else return -1;
	}
}
