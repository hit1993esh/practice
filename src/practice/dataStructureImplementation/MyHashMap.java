package practice.dataStructureImplementation;

import java.util.ArrayList;
import java.util.Objects;

public class MyHashMap<K, V> {

	static class HashNode<K, V> {
		K key;
		V value;
		final int hash;

		HashNode<K, V> next;

		public HashNode(K key, V value, int hash) {
			this.key = key;
			this.value = value;
			this.hash = hash;
		}
	}

	private ArrayList<HashNode<K, V>> bucketArray;

	private int capacity;
	private double loadFactor;

	private int size;

	public MyHashMap() {
		bucketArray = new ArrayList<>();
		capacity = 16;
		size = 0;
		loadFactor = 0.75;

		for (int i = 0; i < capacity; i++)
			bucketArray.add(null);
	}
	public MyHashMap(int capacity, double loadFactor) {
		this.bucketArray = new ArrayList<>();
		this.capacity = capacity;
		this.size = 0;
		this.loadFactor = loadFactor;

		for (int i = 0; i < capacity; i++)
			bucketArray.add(null);
	}

	public int size() {
		return size;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	private final int hashCode(K key) {
		return Objects.hashCode(key);
	}

	private int getBucketIndex(K key) {
		int hashCode = hashCode(key);
		int index = hashCode % capacity;

		index = index < 0 ? index * -1 : index;
		return index;
	}

	public V get(K key) {

		int bucketIndex = getBucketIndex(key);
		int hashCode = hashCode(key);

		HashNode<K, V> head = bucketArray.get(bucketIndex);

		while (head != null) {
			if (head.key.equals(key) && head.hash == hashCode)
				return head.value;

			head = head.next;
		}
		return null;
	}

	public void put(K key, V value) {

		int bucketIndex = getBucketIndex(key);
		int hashCode = hashCode(key);
		HashNode<K, V> head = bucketArray.get(bucketIndex);

		while (head != null) {
			if (head.key.equals(key) && head.hash == hashCode) {
				head.value = value;
				return;
			}
			head = head.next;
		}

		size++;
		head = bucketArray.get(bucketIndex);
		HashNode<K, V> newNode = new HashNode<K, V>(key, value, hashCode);
		newNode.next = head;
		bucketArray.set(bucketIndex, newNode);

		if ((0.1 * size) >= (capacity * 0.75)) {
			ArrayList<HashNode<K, V>> temp = bucketArray;
			bucketArray = new ArrayList<>();
			capacity = 2 * capacity;
			size = 0;
			for (int i = 0; i < capacity; i++)
				bucketArray.add(null);

			for (HashNode<K, V> headNode : temp) {
				while (headNode != null) {
					put(headNode.key, headNode.value);
					headNode = headNode.next;
				}
			}
		}
	}

	public V remove(K key) {

		int bucketIndex = getBucketIndex(key);
		int hashCode = hashCode(key);

		HashNode<K, V> head = bucketArray.get(bucketIndex);

		HashNode<K, V> prev = null;
		while (head != null) {

			if (head.key.equals(key) && hashCode == head.hash)
				break;

			prev = head;
			head = head.next;
		}

		if (head == null)
			return null;

		size--;

		if (prev != null)
			prev.next = head.next;
		else
			bucketArray.set(bucketIndex, head.next);

		return head.value;
	}
	
	public String toString() {
		
		String res = "[";
		
		for(int i = 0 ;i < capacity ; i ++) {
			if(bucketArray.get(i) != null) {
				HashNode<K, V> head = bucketArray.get(i);
				while(head != null) {
					res += " " + head.key + "=" + head.value + " ";
					head = head.next;
				}
				
			}
		}
		return res + "]";
	}
	
	 public static void main(String[] args)
	    {
		 	MyHashMap<String, Integer> map = new MyHashMap<>();
	        map.put("one", 1);
	        map.put("two", 2);
	        map.put("three", 3);
	        map.put("four", 4);
	        
	        System.out.println(map);
	       
	    }

}
